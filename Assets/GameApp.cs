﻿using System.Collections;
using GameSystem;
using UnityEngine;

public class GameApp : MonoBehaviour
{
    private static SystemTimer systemTimer = new SystemTimer();

    public static SystemTimer SystemTimer => systemTimer;

    private static PlayerInputManger
        playerInputManger = new PlayerInputManger();

    public static PlayerInputManger PlayerInputManger => playerInputManger;

    void onStart()
    {

    }
    // Update is called once per frame
    void Update()
    {
        systemTimer.Update();
        PlayerInputManger.Update();
    }

    public static GameController GameController;
}
