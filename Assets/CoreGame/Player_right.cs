﻿using System.Collections;
using System.Collections.Generic;
using CoreGame;
using GameSystem;
using Spine.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.CoreGame
{
    public class Player_right : MonoBehaviour
    {
        public AudioSource musicPlayer;

        public AudioClip clip_XI;

        public AudioClip clip_FANG;

        public int LifeNum = 5;

        public float dianliang = 200;

        public GameObject player_Left_Obj;

        public GameObject player_Right_Obj;

        public GameObject Fx_Right;

        private IAlgorithm algorithmAdd;

        private IAlgorithm algorithmSubtraction;

        Player_left player_Left;

        public StateMachine<int> stateMachine;

        private const int KEY_NONE = 0;

        private const int KEY_O = 1;

        private const int KEY_P = 2;

        private Queue<int> KeyInputQueue = new Queue<int>(2);

        private SkeletonGraphic Fx_Right_cs;

        private void Start()
        {
            Fx_Right_cs = Fx_Right.GetComponent<SkeletonGraphic>();
            Fx_Right_cs.AnimationState.SetAnimation(0, "dianchi", true);
            stateMachine = new StateMachine<int>();
            stateMachine.AddState(PlayerState.idle, null, null, null);
            stateMachine.AddState(PlayerState.absorb, null, null, null);
            stateMachine.AddState(PlayerState.discharge, null, null, null);
            stateMachine.SwitchTo(PlayerState.idle);

            KeyInputQueue.Enqueue (KEY_NONE);

            player_Left = player_Left_Obj.GetComponent<Player_left>();
            algorithmAdd = new CoreGame.Alforithm.AddAlforithm();
            algorithmSubtraction =
                new CoreGame.Alforithm.SubtractionAlforithm();

            GameApp
                .PlayerInputManger
                .AddListener(KeyCode.O,
                new GameSystem.ListenKeyDownDuraction(key_O_Down),
                new GameSystem.ListenKeyDownDuraction(key_O_DownIng),
                new GameSystem.ListenKeyDownDuraction(key_O_Up));

            GameApp
                .PlayerInputManger
                .AddListener(KeyCode.P,
                new GameSystem.ListenKeyDownDuraction(key_P_Down),
                new GameSystem.ListenKeyDownDuraction(key_P_DownIng),
                new GameSystem.ListenKeyDownDuraction(key_P_Up));
        }

        // private float time_XI = 2.0F;
        private int time_FANG;

        public void playMusic(AudioClip clip)
        {
            if (!musicPlayer.isPlaying)
            {
                musicPlayer.clip = clip;
                musicPlayer.loop = false;
                musicPlayer.Play();
            }
        }

        private void key_O_Down(KeyCode keyCode, float duraction)
        {
            if (KeyInputQueue.Peek() == KEY_NONE)
            {
                stateMachine.SwitchTo(PlayerState.absorb);
                KeyInputQueue.Enqueue (KEY_O);
                KeyInputQueue.Dequeue();
            }
            else if (
                KeyInputQueue.Peek() == KEY_P && !KeyInputQueue.Contains(KEY_O)
            )
            {
                KeyInputQueue.Enqueue (KEY_O);
            }
        }

        float time;

        private void key_O_DownIng(KeyCode keyCode, float duraction)
        {
            if (KeyInputQueue.Peek() == KEY_O)
            {
                if (stateMachine.CurrState != PlayerState.absorb)
                    stateMachine.SwitchTo(PlayerState.absorb);

                //吸电
                playMusic (clip_XI);
                if (
                    Fx_Right_cs.AnimationState.GetCurrent(0).Animation.Name !=
                    "xi"
                ) Fx_Right_cs.AnimationState.SetAnimation(0, "xi", true);

                time += Time.deltaTime;
                if (time >= 0.02F)
                {
                    OnAdd();
                    time = 0;
                }
            }
        }

        private void key_O_Up(KeyCode keyCode, float duraction)
        {
            if (KeyInputQueue.Peek() == KEY_O)
            {
                stateMachine.SwitchTo(PlayerState.idle);
                KeyInputQueue.Dequeue();
                if (KeyInputQueue.Count == 0)
                {
                    Fx_Right_cs.AnimationState.SetAnimation(0, "dianchi", true);
                    KeyInputQueue.Enqueue (KEY_NONE);
                }
            }
            else if (KeyInputQueue.Contains(KEY_O))
            {
                var key = KeyInputQueue.Dequeue();
                KeyInputQueue.Dequeue();
                KeyInputQueue.Enqueue (key);
            }
        }

        private void key_P_Down(KeyCode keyCode, float duraction)
        {
            if (KeyInputQueue.Peek() == KEY_NONE)
            {
                stateMachine.SwitchTo(PlayerState.discharge);
                KeyInputQueue.Enqueue (KEY_P);
                KeyInputQueue.Dequeue();
            }
            else if (
                KeyInputQueue.Peek() == KEY_O && !KeyInputQueue.Contains(KEY_P)
            )
            {
                KeyInputQueue.Enqueue (KEY_P);
            }
        }

        float _time;

        private void key_P_DownIng(KeyCode keyCode, float duraction)
        {
            //吸电
            playMusic (clip_FANG);
            if (KeyInputQueue.Peek() == KEY_P)
            {
                if (stateMachine.CurrState != PlayerState.discharge)
                    stateMachine.SwitchTo(PlayerState.discharge);
                if (
                    Fx_Right_cs.AnimationState.GetCurrent(0).Animation.Name !=
                    "fang"
                ) Fx_Right_cs.AnimationState.SetAnimation(0, "fang", true);
                _time += Time.deltaTime;
                if (_time >= 0.02F)
                {
                    OnSubtraction (duraction);
                    _time = 0;
                }
            }
        }

        private void key_P_Up(KeyCode keyCode, float duraction)
        {
            if (KeyInputQueue.Peek() == KEY_P)
            {
                stateMachine.SwitchTo(PlayerState.idle);
                KeyInputQueue.Dequeue();
                if (KeyInputQueue.Count == 0)
                {
                    Fx_Right_cs.AnimationState.SetAnimation(0, "dianchi", true);
                    KeyInputQueue.Enqueue (KEY_NONE);
                }
            }
            else if (KeyInputQueue.Contains(KEY_P))
            {
                var key = KeyInputQueue.Dequeue();
                KeyInputQueue.Dequeue();
                KeyInputQueue.Enqueue (key);
            }
        }

        private void OnAdd()
        {
            float num;
            num = Random.Range(1f, 6f);
            this.dianliang += num;
            player_Left.dianliang -= num;
            // var addNum = algorithmAdd?.Formula(duraction);
            // GameApp.SystemTimer.ResisteredTimer(OnAddXI, time_XI, 1);

            // Debug.LogError(addNum.ToString());
        }

        private void OnAddXI()
        {
            this.dianliang += 2;
        }

        private void OnSubtraction(float duraction)
        {
            var subtractionNum = algorithmSubtraction.Formula(duraction);

            this.dianliang -= subtractionNum;

            if (player_Left.stateMachine.CurrState == PlayerState.absorb)
            {
                player_Left.dianliang += (float) subtractionNum;
                player_Left.dianliangMax -= 8 * (float) subtractionNum;
            }

            // Debug
            //     .LogError($"OnSubtraction : {player_Left.stateMachine.CurrState}");
        }

        public GameObject fatherRight;

        public float dianliangMax;

        void Update()
        {
            if (LifeNum <= 0) return;
            fatherRight.GetComponent<RectTransform>().sizeDelta =
                new Vector2(100, dianliangMax / 1000 * 235);
            this.GetComponent<RectTransform>().sizeDelta =
                new Vector2(100, dianliang / 1000 * 235);

            SubDL();
            DlMax();
            JieSuan();
        }

        float _timeT;

        private void JieSuan()
        {
            if (this.dianliang <= 0)
            {
                GameOver();
            }
        }

        bool isDead;

        private void GameOver()
        {
            if (LifeNum >= 1 && !isDead)
            {
                LifeNum -= 1;
                if (LifeNum <= 0)
                {
                    EndGame();
                    return;
                }
                else
                {
                    GameApp.SystemTimer.ResisteredTimer(Reborn, 1, 1);
                    isDead = true;
                }
            }
            if (GameApp.GameController.StateMachine.CurrState == GameState.End)
                return;
            GameApp.GameController.StateMachine.SwitchTo(GameState.BulletTime);
            GameApp
                .SystemTimer
                .ResisteredTimer(() =>
                {
                    GameApp
                        .GameController
                        .StateMachine
                        .SwitchTo(GameState.Noraml);
                },
                2,
                1);
        }

        float dianliangMaxSet = 1000;

        void Reborn()
        {
            isDead = false;
            dianliang = dianliangMaxSet;
            dianliangMax = dianliangMaxSet;
        }

        void StartScene()
        {
            // SceneManager.LoadScene("Start", LoadSceneMode.Single);
            Application.Quit();
        }

        void EndGame()
        {
            AudioSource audioSource =
                GameObject
                    .FindGameObjectWithTag("tem")
                    .GetComponent<AudioSource>();
            audioSource.Stop();
            audioSource.clip =
                GameObject.FindObjectOfType<GameController>().clip;
            audioSource.loop = true;
            audioSource.Play();
            player_Right_Obj.GetComponent<Animator>().Play("move");

            GameApp.GameController.OnLeaveBulletTime();
            GameApp.SystemTimer.Clear();
            GameApp.PlayerInputManger.RemoveListener(KeyCode.O);
            GameApp.PlayerInputManger.RemoveListener(KeyCode.P);
            GameApp
                .SystemTimer
                .ResisteredTimer(() =>
                {
                    GameApp.GameController.StateMachine.SwitchTo(GameState.End);
                    GameApp.SystemTimer.ResisteredTimer(StartScene, 13, 1);
                },
                3.6f,
                1);
        }

        private void SubDL()
        {
            _timeT += Time.deltaTime;
            if (_timeT >= 0.02F)
            {
                this.dianliang -= 0.06F;
                _timeT = 0;
            }
        }

        private void DlMax()
        {
            if (this.dianliang >= this.dianliangMax)
            {
                this.dianliang = this.dianliangMax;
            }
        }
    }
}
