﻿using System.Collections;
using UnityEngine;

namespace Assets.CoreGame
{
    public static class GameState
    {
        /// <summary>
        /// 默认
        /// </summary>
        public const int Noraml = 0;
        /// <summary>
        /// 子弹时间
        /// </summary>
        public const int BulletTime = 1;
        /// <summary>
        /// 结束
        /// </summary>
        public const int End = 2;

    }
}