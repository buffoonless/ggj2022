﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Guide : MonoBehaviour
{
    public Sprite[] guides;
    // Start is called before the first frame update
    private int _step;
    private Transform _imageTrans;
    void Start()
    {
        this.transform.position = new Vector3(0, 0, 1);
        this._imageTrans = this.transform.Find("image");
        if (!this._imageTrans)
        {
            GameObject imgObj = new GameObject("image");
            imgObj.transform.parent = this.transform;
            imgObj.transform.localPosition = new Vector3(0, 0, 0);
            imgObj.AddComponent<SpriteRenderer>();
            this._imageTrans = imgObj.transform;
        }
        this._step = 0;
        nextGuide();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            nextGuide();
        }
    }

    bool nextGuide()
    {
        if (this._step >= guides.Length)
        {
            GameStart.showNext();
            return false;
        }
        this._imageTrans.GetComponent<SpriteRenderer>().sprite = guides[this._step];
        this._step++;
        return true;
    }

}
