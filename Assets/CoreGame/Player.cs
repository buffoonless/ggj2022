﻿using System.Collections;
using CoreGame;
using UnityEngine;

namespace Assets.CoreGame
{
    public class Player : MonoBehaviour
    {
        private IAlgorithm algorithmAdd;

        private IAlgorithm algorithmSubtraction;

        void Start()
        {
            algorithmAdd = new CoreGame.Alforithm.AddAlforithm();
            algorithmSubtraction =
                new CoreGame.Alforithm.SubtractionAlforithm();

            GameApp
                .PlayerInputManger
                .AddListener(KeyCode.E,
                new GameSystem.ListenKeyDownDuraction(OnAdd),
                new GameSystem.ListenKeyDownDuraction(OnAdd),
                new GameSystem.ListenKeyDownDuraction(OnAdd));
            GameApp
                .PlayerInputManger
                .AddListener(KeyCode.R,
                new GameSystem.ListenKeyDownDuraction(OnSubtraction),
                new GameSystem.ListenKeyDownDuraction(OnSubtraction),
                new GameSystem.ListenKeyDownDuraction(OnSubtraction));
        }

        private void OnAdd(KeyCode keyCode, float duraction)
        {
            var addNum = algorithmAdd?.Formula(duraction);
            Debug.LogError(addNum.ToString());
        }

        private void OnSubtraction(KeyCode keyCode, float duraction)
        {
            var subtractionNum = algorithmSubtraction?.Formula(duraction);
            Debug.LogError(subtractionNum.ToString());
        }
    }
}
