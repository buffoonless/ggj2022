﻿using System.Collections;
using Assets.CoreGame;
using UnityEngine;

namespace CoreGame
{
    public class BGADPlayer : MonoBehaviour, IAniamtion
    {
        public GameObject[] gameObjectADs;

        public System.Random random = new System.Random();

        void Start()
        {
            GameApp
                .SystemTimer
                .ResisteredTimer(() =>
                {
                    RandomPaly();
                },
                random.Next(1000, 3000) * 0.001f,
                1);
        }

        private void RandomPaly()
        {
            if (GameApp.GameController != null && GameApp.GameController.StateMachine.CurrState == GameState.End)
                return;
            if (gameObjectADs.Length <= 0) return;
            var randomIndex = random.Next(0, gameObjectADs.Length);
            var obj = gameObjectADs[randomIndex];
            obj.GetComponent<Animator>().Play("move");
            GameApp
                .SystemTimer
                .ResisteredTimer(() =>
                {
                    RandomPaly();
                },
                random.Next(1000, 3000) * 0.001f,
                1);
        }

        public void SetSpeed(float scale)
        {
            for (int i = gameObjectADs.Length - 1; i >= 0; i--)
            {
                gameObjectADs[i].GetComponent<Animator>().speed = scale;
            }
        }
    }
}
