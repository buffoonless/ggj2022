﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStart : MonoBehaviour
{
    public static GameStart s_gs;

    public GameObject[] objs;

    private int _step;

    private GameObject _cloneObj;

    // Start is called before the first frame update
    //音乐播放器
    public AudioSource musicPlayer;

    public AudioSource soundPlayer;

    void Start()
    {
        s_gs = this;
        this._step = 0;
        this.next();
        playMusic("Sound/bg");
    }

    // Update is called once per frame
    void Update()
    {
    }

    bool next()
    {
        if (this._step >= objs.Length)
        {
            Destroy(this);
            return false;
        }
        if (this._cloneObj)
        {
            Destroy(this._cloneObj);
        }
        this._cloneObj = Instantiate(objs[this._step], this.transform);
        this._cloneObj.transform.position = new Vector3(0, 0, 1);
        this._step++;
        return true;
    }

    //跳到下一个步骤
    public static void showNext()
    {
        s_gs.next();
    }

    //播放音乐
    public void playMusic(string name)
    {
        if (musicPlayer.isPlaying)
        {
            musicPlayer.Stop();
        }
        AudioClip clip = Resources.Load<AudioClip>(name);
        musicPlayer.clip = clip;
        musicPlayer.playOnAwake = true;
        musicPlayer.loop = false;
        musicPlayer.Play();
        GameApp.SystemTimer.ResisteredTimer(StopMusic, 5, 1);
    }

    void StopMusic()
    {
        musicPlayer.Stop();
    }

    //音效
    public void playSound(string name)
    {
        AudioClip clip = Resources.Load<AudioClip>(name);
        soundPlayer.clip = clip;
        soundPlayer.playOnAwake = false;
        soundPlayer.Play();
    }
}
