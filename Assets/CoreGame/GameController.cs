using System.Collections;
using System.Collections.Generic;
using Assets.CoreGame;
using CoreGame;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public AudioClip clip;

    public GameObject bgAdObj;

    public GameObject bgObj;

    public GameObject finalObj;

    IAniamtion BGAdAni;

    IAniamtion BGShakerAni;

    GameSystem.StateMachine<int>
        stateMachine = new GameSystem.StateMachine<int>();

    public GameSystem.StateMachine<int> StateMachine => stateMachine;

    void Start()
    {
        GameApp.GameController = this;
        finalObj.SetActive(false);
        BGAdAni = bgAdObj.GetComponent<CoreGame.BGADPlayer>();
        BGShakerAni = bgObj.GetComponent<BGShaker>();
        (BGShakerAni as BGShaker).init();
        stateMachine.AddState(GameState.Noraml, null, null, null);
        stateMachine
            .AddState(GameState.BulletTime,
            OnEnterBulletTime,
            null,
            OnLeaveBulletTime);
        stateMachine.AddState(GameState.End, OnEnterEnd, null, null);
        stateMachine.SwitchTo(GameState.Noraml);
    }

    private void OnEnterBulletTime()
    {
        BGAdAni.SetSpeed(0.1f);
        BGShakerAni.SetSpeed(0.1f);
    }

    public void OnLeaveBulletTime()
    {
        BGAdAni.SetSpeed(1f);
        BGShakerAni.SetSpeed(1f);
    }

    private void OnEnterEnd()
    {
        BGAdAni.SetSpeed(1f);
        BGShakerAni.SetSpeed(1f);
        finalObj.SetActive(true);
        finalObj.transform.GetChild(0).GetComponent<Animator>().Play("final");
    }
}
