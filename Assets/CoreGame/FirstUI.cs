﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstUI : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer[] sps;
    [SerializeField]
    float delayTime = 2.0f;
    [SerializeField]
    float showTime = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < sps.Length; i++)
        {
            sps[i].color = new Color(1, 1, 1, 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        delayTime -= Time.deltaTime;
        if (delayTime <= 0)
        {
            GameStart.showNext();
        }
        for (int i = 0; i < sps.Length; i++)
        {
            sps[i].color = new Color(1, 1, 1, sps[i].color.a + Time.deltaTime / showTime);
        }

    }
}
