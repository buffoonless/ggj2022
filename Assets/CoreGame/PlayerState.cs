﻿using System.Collections;
using UnityEngine;

namespace Assets.CoreGame
{
    public static class PlayerState
    {
        /// <summary>
        /// 默认
        /// </summary>
        public const int idle = 0;
        /// <summary>
        /// 吸收
        /// </summary>
        public const int absorb = 1;
        /// <summary>
        /// 放电
        /// </summary>
        public const int discharge = 2;

    }
    
    public static class KeyDownState
    {
        /// <summary>
        /// 默认
        /// </summary>
        public const int idle = 0;
        /// <summary>
        /// 吸收
        /// </summary>
        public const int downing = 1;
    }
}