﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeMove : MonoBehaviour
{
    [SerializeField]
    float delay;
    [SerializeField]
    float time;
    [SerializeField]
    Vector3 byPos;
    private float _intime;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _intime += Time.deltaTime;
        if (_intime < time + delay && _intime > delay)
        {
            this.transform.position = this.transform.position + byPos * Time.deltaTime / time;

            return;
        }
    }
}
