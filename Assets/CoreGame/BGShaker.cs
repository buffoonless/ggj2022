﻿using CoreGame;
using System.Collections;
using UnityEngine;

namespace Assets.CoreGame
{
    public class BGShaker : MonoBehaviour, IAniamtion
    {

        public GameObject leftPalyer;

        public GameObject rightPlayer;

        private Animator leftAni;

        private Animator rightAni;

        public System.Random random = new System.Random();

        private Animator Animator;

        public void init()
        {
            Animator = transform.GetComponent<Animator>();
            leftAni = leftPalyer.GetComponent<Animator>();
            rightAni = rightPlayer.GetComponent<Animator>();
            GameApp.SystemTimer.ResisteredTimer(() =>
            {
                RandomPaly();
            }, random.Next(1000, 3000) * 0.001f, 1);
        }

        private void RandomPaly()
        {
            Animator.Play("shaker");
            leftAni.Play("shake");
            rightAni.Play("shake");
            GameApp.SystemTimer.ResisteredTimer(() =>
            {
                RandomPaly();
            }, random.Next(3000, 6000) * 0.001f, 1);
        }

        public void SetSpeed(float scale)
        {
            Animator.speed = scale;
            leftAni.speed = scale;
            leftAni.speed = scale;
        }
    }
}