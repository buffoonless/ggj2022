﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnGComp : MonoBehaviour
{
    [SerializeField]
    string keyCode;
    [SerializeField]
    Sprite upImg;
    [SerializeField]
    Sprite downImg;

    private Transform _imageTrans;
    // Start is called before the first frame update
    void Start()
    {
        this._imageTrans = this.transform.Find("image");
        if (!this._imageTrans)
        {
            GameObject imgObj = new GameObject("image");
            imgObj.AddComponent<UnityEngine.UI.Image>();
            imgObj.transform.parent = this.transform;
            this._imageTrans = imgObj.transform;
        }
        this._imageTrans.GetComponent<UnityEngine.UI.Image>().sprite = upImg;
        this._imageTrans.localPosition = Vector3.zero;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(keyCode))
        {
            this._imageTrans.GetComponent<UnityEngine.UI.Image>().sprite = downImg;
            this._imageTrans.GetComponent<UnityEngine.UI.Image>().SetNativeSize();
        }
        if (Input.GetKeyUp(keyCode))
        {
            this._imageTrans.GetComponent<UnityEngine.UI.Image>().sprite = upImg;
            this._imageTrans.GetComponent<UnityEngine.UI.Image>().SetNativeSize();
        }

    }

}
