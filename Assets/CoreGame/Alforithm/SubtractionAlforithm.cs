﻿using System.Collections;
using CoreGame;
using UnityEngine;

namespace Assets.CoreGame.Alforithm
{
    public class SubtractionAlforithm : IAlgorithm
    {
        public float Formula(float inputDuraction)
        {
            return Mathf.Max(inputDuraction * inputDuraction / 2 + 1, 1f);
        }
    }
}
