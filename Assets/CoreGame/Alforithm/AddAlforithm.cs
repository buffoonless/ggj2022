﻿using System.Collections;
using CoreGame;
using UnityEngine;

namespace Assets.CoreGame.Alforithm
{
    public class AddAlforithm : IAlgorithm
    {
        public float Formula(float inputDuraction)
        {
            return inputDuraction + 1;
        }
    }
}
