﻿using System.Collections;
using UnityEngine;

namespace CoreGame
{
    interface IAlgorithm
    {
        float Formula(float inputDuraction);
    }
}
