﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    public class StateMachine<T> where T : IComparable<T>
    {
        private Dictionary<T, State> states = new Dictionary<T, State>(8);

        private State currState;

        public T CurrState
        {
            get
            {
                if (currState != null) return currState.id;
                return default;
            }
        }

        public void AddState(
            T stateId,
            Action Enter,
            Action update,
            Action leave
        )
        {
            if (states.TryGetValue(stateId, out var state))
            {
                state.onEnter = Enter;
                state.onUpdate = update;
                state.onLeave = leave;
            }
            else
            {
                states
                    .Add(stateId,
                    new State()
                    {
                        id = stateId,
                        onEnter = Enter,
                        onUpdate = update,
                        onLeave = leave
                    });
            }
        }

        public void SwitchTo(T newStateId)
        {
            if (
                states.TryGetValue(newStateId, out var newState) &&
                currState != newState
            )
            {
                if (currState != null) currState.onLeave?.Invoke();
                currState = newState;
                newState.onEnter?.Invoke();
            }
        }

        public void Update()
        {
            currState?.onUpdate();
        }

        private class State
        {
            public T id;

            public Action onEnter;

            public Action onUpdate;

            public Action onLeave;
        }
    }
}
