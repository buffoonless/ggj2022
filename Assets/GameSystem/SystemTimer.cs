﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    public class SystemTimer
    {
        private class TimerCell
        {
            public uint Tag { private set; get; }
            public Action CallBack { private set; get; }

            public float initRest { private set; get; }

            public float rest;

            private int times;

            private int excutedTimes;

            private SystemTimer systemTimer;

            public bool isExpire;
            public void init(SystemTimer systemTimer, Action callBack, uint tag, float duraction, int times)
            {
                this.systemTimer = systemTimer;
                Tag = tag;
                initRest = duraction;
                rest = duraction;
                this.times = times;
                excutedTimes = 0;
                CallBack = callBack;
            }

            public bool Update(float duraction)
            {
                if (isExpire)
                    return isExpire;
                rest -= duraction;
                if(rest <= 0)
                {
                    excutedTimes++;
                    if (excutedTimes >= times)
                    {
                        isExpire = true;
                    }
                    else
                    {
                        rest = initRest;
                    }
                    CallBack?.Invoke();
                }
                return isExpire;
            }

            public void Expire()
            {
                systemTimer.RemoveTimer(Tag);
            }
        }

        private List<TimerCell> timerCells = new List<TimerCell>(64);

        private Dictionary<uint, int> cacheTimerCells = new Dictionary<uint, int>(64);

        private uint tag = 0;
        public SystemTimer()
        {

        }        

        public void Update()
        {
            var detail = Time.deltaTime;
            var deleteCount = 0;
            var length = timerCells.Count;
            for (int i = 0; i < length; i++)
            {
                var cell = timerCells[i];
                if (cell.Update(detail))
                {
                    deleteCount++;
                    RemoveTimerTag(cell.Tag);
                }
                else
                {
                    timerCells[i - deleteCount] = cell;
                }           
            }

            if(deleteCount > 0)
            {
                timerCells.RemoveRange(length - deleteCount, deleteCount);
            }
            if(timerCells.Count > 0)
            {
                timerCells.Sort(CompateTimer);
                length = timerCells.Count;
                for (int i = 0; i < length; i++)
                {
                    var cell = timerCells[i];
                    cacheTimerCells[cell.Tag] = i;
                }
            }
        }

        public uint ResisteredTimer(Action callBack, float duraction, int times)
        {
            tag++;
            var timercell = new TimerCell();
            timercell.init(this, callBack, tag, duraction, times);
            timerCells.Add(timercell);
            cacheTimerCells.Add(tag, timerCells.Count);
            return tag;
        }

        public void RemoveTimer(uint tag)
        {
            if(cacheTimerCells.TryGetValue(tag, out var index))
            {
                timerCells[index].isExpire = true;
                cacheTimerCells.Remove(tag);
            }
        }

        private void RemoveTimerTag(uint tag)
        {
            cacheTimerCells.Remove(tag);
        }

        private int CompateTimer(TimerCell x, TimerCell y)
        {
            var dif = x.rest - y.rest;
            if (dif == 0)
                return 0;
            return dif > 0 ? 1 : -1;
        }

        public void Clear()
        {
            cacheTimerCells.Clear();
            timerCells.Clear();
        }
    }
}

