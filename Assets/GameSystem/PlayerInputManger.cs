﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    public delegate void ListenKeyDownDuraction(KeyCode code, float duraction);

    public delegate void ListenKeyUpDuraction(KeyCode code, float duraction);

    public delegate void ListenKeyDownKeepDuraction(KeyCode code, float duraction);

    public class PlayerInputManger
    {

        class Listeners
        {
            public List<ListenKeyDownDuraction> down;
            public List<ListenKeyDownDuraction> keep;
            public List<ListenKeyDownDuraction> up;
        }

        private Dictionary<KeyCode, Listeners> listeners = new Dictionary<KeyCode, Listeners>(16);
        private Dictionary<KeyCode, (bool, float)> keyDownCounter = new Dictionary<KeyCode, (bool, float)>(16);
        public void Update()
        {
            var currTime = Time.time;
            foreach (var item in listeners)
            {
                if(Input.GetKeyDown(item.Key))
                {
                    if (keyDownCounter.TryGetValue(item.Key, out var tuple))
                    {
                        tuple.Item1 = true;
                        tuple.Item2 = currTime;
                        keyDownCounter[item.Key] = tuple;
                    }
                    else
                    {
                        tuple.Item1 = true;
                        tuple.Item2 = currTime;
                        keyDownCounter.Add(item.Key, tuple);
                    }
                    foreach (var callBack in item.Value.down)
                    {
                        callBack(item.Key, 0);
                    }
                }
                else if (Input.GetKeyUp(item.Key))
                {
                    
                    if (keyDownCounter.TryGetValue(item.Key, out var tuple))
                    {
                        if (tuple.Item1)
                        {
                            foreach (var callBack in item.Value.up)
                            {
                                callBack(item.Key, currTime - tuple.Item2);
                            }
                        }
                        keyDownCounter.Remove(item.Key);
                    }
                }
                else
                {
                    if (keyDownCounter.TryGetValue(item.Key, out var tuple))
                    {
                        if (tuple.Item1)
                        {
                            foreach (var callBack in item.Value.keep)
                            {
                                callBack(item.Key, currTime - tuple.Item2);
                            }
                        }
                    }
                }
            }
        }
    
        public void AddListener(KeyCode keyCode, 
            ListenKeyDownDuraction listenKeyDownDuraction, 
            ListenKeyDownDuraction listenKeyDownKeepDuraction,
            ListenKeyDownDuraction listenKeyUpDuraction
        )
        {
            if(listeners.TryGetValue(keyCode,out var Listendelegates))
            {
                if(!Listendelegates.down.Contains(listenKeyDownDuraction))
                {
                    Listendelegates.down.Add(listenKeyDownDuraction);

                }
                if (!Listendelegates.keep.Contains(listenKeyDownKeepDuraction))
                {
                    Listendelegates.keep.Add(listenKeyDownKeepDuraction);
                }
                if (!Listendelegates.up.Contains(listenKeyUpDuraction))
                {
                    Listendelegates.up.Add(listenKeyUpDuraction);
                }
            }
            else
            {
                listeners.Add(keyCode, new Listeners()
                {
                    down = new List<ListenKeyDownDuraction>(8) { listenKeyDownDuraction },
                    keep = new List<ListenKeyDownDuraction>(8) { listenKeyDownKeepDuraction },
                    up = new List<ListenKeyDownDuraction>(8) { listenKeyUpDuraction },
                });
            }
        }

        public void RemoveListener(KeyCode keyCode)
        {
            if (listeners.TryGetValue(keyCode, out var Listendelegates))
            {
                Listendelegates.down.Clear();
                Listendelegates.keep.Clear();
                Listendelegates.up.Clear();
            }
        }


        public void RemoveDownListener(KeyCode keyCode, ListenKeyDownDuraction listenKeyDownDuraction)
        {
            if (listeners.TryGetValue(keyCode, out var Listendelegates))
            {
                if (!Listendelegates.down.Contains(listenKeyDownDuraction))
                {
                    Listendelegates.down.Add(listenKeyDownDuraction);

                }
            }
        }

        public void RemoveDownKeepListener(KeyCode keyCode, ListenKeyDownDuraction listenKeyDownKeepDuraction)
        {
            if (listeners.TryGetValue(keyCode, out var Listendelegates))
            {
                if (!Listendelegates.keep.Contains(listenKeyDownKeepDuraction))
                {
                    Listendelegates.keep.Add(listenKeyDownKeepDuraction);
                }
            }
        }

        public void RemoveUpListener(KeyCode keyCode, ListenKeyDownDuraction listenKeyUpDuraction)
        {
            if (listeners.TryGetValue(keyCode, out var Listendelegates))
            {
                if (!Listendelegates.up.Contains(listenKeyUpDuraction))
                {
                    Listendelegates.up.Add(listenKeyUpDuraction);
                }
            }
        }
    }
}